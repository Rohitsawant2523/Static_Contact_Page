import React from 'react'
import { Link } from 'react-router-dom'
const ContactPage = () => {
    return (
        <>
            <div className='container-fluid'>
                <div className='row justify-content-center' >
                    <div className='contact_main_div col-md-5'>
                        <div className='contact_image mt-3'>
                            <h1>D</h1>
                        </div>
                        <div className='contact_info text-center mb-3'>
                            <h5>Dinesh Hamirwasia</h5>
                            {/* <p>Director - NNM group</p> */}
                        </div>
                        <div className='form_input'>
                            <span className='label_input'>Phone (Mobile)</span>
                            <div className='input_field' >
                                <a href="/contact" className='link_text'>+91 9702020278</a>
                            </div>
                        </div>
                        <div className='form_input'>
                            <span className='label_input'>Email (Work)</span>
                            <div className='input_field' >
                                <a href="https://mail.google.com/mail/u/0/?tab=rm&ogbl#inbox?compose=new" className='link_text' target='_blank'>dinesh@cokaco.com</a>
                            </div>
                        </div>
                        <div className='form_input'>
                            <span className='label_input'>Address</span>
                            <textarea type="text" className='input_field' rows="6"
                                defaultValue={`NNM Securities Pvt. Ltd.\nB-6/7, Shree Siddhivinayak Plaza off\nNew Link Road, Andheri-west\nMaharashtra\n400053\nIndia`}
                            >
                            </textarea>
                        </div>
                    </div>
                </div>
            </div>
        </>
    )
}

export default ContactPage